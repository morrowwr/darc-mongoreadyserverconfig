# Introduction

Package to handle flexible configuration for the `darc-mongoreadyserver` package. Our intention is that it should be easy to run a server, from the command line, based on this package. 

You can specify environment variables, either with `export` or with a `.env` file in your project's root directory. All environment variables should be preceeded by your project name, in upper case, with any `-` characters replaced with `_`, followed by an `_`, and then the variable name. For example, 

```
MYPROJECT_CUSTOM_ENV_VAR="Hi!"
```

This will be read into the configuration object with the key `custom-env-var`. Of course, the variables can also be the defaults required by the package, as in

```
MYPROJECT_PORT=6000
MYPROJECT_SSL_CERT=/path/to/sssl/cert
MYPROJECT_SSL_KEY=/path/to/ssl/key
```

You can specify a configuration file, currently in `JSON` form. You can set an environment variable `MYPROJECT_CONFFILE` to point to this file, or you can use the command line argument '--conffile'. Such a configuration file will be read and imported into the configuration directly. 

You can also run with long-form command line arguments (run with `--help` to see which), and you can even specify your _own_ command line arguments (and value checks). Our examples here show how to use command line arguments; to add custom arguments you need to pass arguments `optargs` and/or `checks` to the routine returned from the `require` call: 

```
require( 'darc-monngoreadyserver' )( modules , optargs , checks )
	...
```

`optargs` should be an object whose keys are the long-form options to include (minus the `--` prefix), and whose corresponding values should be [`argparse`](https://www.npmjs.com/package/argparse)-ready option setting objects. For example, 

```
let optargs = {
	'custom-flag' : { help : 'An extra flag' , action : "storeTrue" , default : false } , 
	'custom-arg' : { help : 'An extra argument' , default : 7 } , 
}
```

See the `argparse` docs for more details. `checks`, if provided, should be an object whose keys are the argument names (e.g., `custom-flag`) and whose values are strings that can be executed as an [`eval`]() statement over a single variable `val` returning a boolean. For example, 

```
let checks = { 
	'custom-arg' : 'val >= 2'
}
```

These different routes have a specific precedence order: environment variables are overwritten by config file values which are overwritten by command line arguments (to the degree there are any conflicts). The only exception is a config file path itself, which is set to any value from the environment overwritten by any command line argument provided. 