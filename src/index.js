/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * DARC MONGODB READY SERVER CONFIGURATION
 * 
 * This code provides a uniform configuration wrapper for use with backend server projects. 
 * 
 * Rough usage: 
 * 
 * 		const _config = require('darc-mongoreadyserverconfig')( "TOOLPREFIX" , "some backend tool" );
 * 
 * Copyright Stanford GSB DARC Team 
 * 
 * Created by W. Ross Morrow, Research Computing Specialist
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const _fs = require( 'fs' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * LOCAL DATA
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const CONFIGURED_DEFAULT_PORT = 5000;

// these are environment-variable style forms of the default variables
/*
const defvars = [
	"PORT"   , "SSL_CERT" , "SSL_KEY"   , "SSL_CHAIN" , 
	"MDB_DB" , "MDB_URI"  , "MDB_NAMES" , "OPTIONS"
];
*/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * UTILITY FUNCTIONS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const protect = ( a , b ) => ( a ? a : b );
const now = () => ( Date.now() / 1000.0 );
const utc = () => ( ( new Date( Date.now() ) ).toISOString() );
const vdk = ( key , dict ) => ( ( key in dict ) && dict[key] );

// convert a JSON-like key/value pair string to a js object
const keyValueStringToObj = ( s ) => {
	var obj = {};
	s.split( ',' ).forEach( kvp => {
		var kav = kvp.split( ':' );
		if( kav.length == 1 ) { obj[kav[0]] = kav[0]; }
		else if( kav.length == 2 ) { obj[kav[0]] = kav[1]; }
		else {
			// technically an error
		}
	} );
	return obj;
}

const isObj = (obj) => ( obj === Object(obj) );

function _dotextract( list , i , val ) {
	let ip1 = i + 1; let result = {}
	if( i + 1 < list.length ) {
		result[list[i]] = _dotextract( list , ip1 , val );
	} else {
		result[list[i]] = val;
	}
	return result;
}

function dotextract( s , val ) {
	return _dotextract( s.split('.') , 0 , val );
}

function inject( obj , data ) {
	Object.keys( data ).forEach( k => {
		if( ! ( k in obj ) ) { obj[k] = data[k]; }
		Object.keys( data[k] ).forEach( j => {
			if( isObj(data[k][j]) ) { 
				if( ! ( j in obj[k] ) ) { obj[k][j] = {}; }
				if( ! isObj( obj[k][j] ) ) { obj[k][j] = { '_' : obj[k][j] }; }
				inject( obj[k][j] , data[k][j] );
			} else {
				obj[k][j] = data[k][j]; 
			}
		} );
	} );
}

// check if a port spec is valid and is open
const isPortValidAndOpen = ( port ) => {
	return new Promise( ( resolve , reject ) => {
		let server = require( 'net' ).createServer();
		server.once( 'error' , ( err ) => {
			console.log( err )
			if( err.code === 'EADDRINUSE' ) { reject(); }
		} );
		server.once( 'listening' , () => { server.close(); resolve(); } );
		server.listen( port );
	} );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * EXPORT A LOAD ROUTINE
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = ( optargs , defaults , checks , nestOn ) => {

	let _config = {};
	let pkg = require( `${process.cwd()}/package.json` ); // get _importing_ project package.json

	// default prefix is the package name
	let prefix = pkg.name.toUpperCase().replace('-','_');

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * 
	 * COMMAND LINE ARGUMENTS
	 * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	// Get CL arguments using ArgumentParser
	var parser = new require( 'argparse' ).ArgumentParser( {
		version : pkg.version ,
		addHelp : true,
		description : pkg.description , 
	} );

	// list of (default?) command line args
	parser.addArgument( '--conf-file' 	, { help: 'Configuration file name' } );
	parser.addArgument( '--port' 		, { help: 'Port server will listen on' } );
	parser.addArgument( '--ssl-cert' 	, { help: 'Location of HTTPS certificate certificate (e.g., cert.pem)' } );
	parser.addArgument( '--ssl-key' 	, { help: 'Location of HTTPS certificate key (e.g., key.pem)' } );
	parser.addArgument( '--ssl-chain' 	, { help: 'Location of HTTPS certificate chain (e.g., chain.pem)' } );
	parser.addArgument( '--mdb-db' 		, { help: 'MongoDB database name' } );
	parser.addArgument( '--mdb-uri' 	, { help: 'MongoDB connection string' } );
	parser.addArgument( '--mdb-names' 	, { help: 'MongoDB collection names, as a JSON-like key-value pair string' } );
	parser.addArgument( '--options' 	, { help: 'A MongoDB ObjectID or a name of the specific options to use' } );
	parser.addArgument( '--dry-run' 	, { help: 'Execute a configuration/system check only' , action: 'storeTrue' } );

	// add any optional arguments here
	if( optargs ) {
		Object.keys( optargs ).forEach( a => {
			parser.addArgument( `--${a}` , optargs[a] );
		} );
	}

	// you COULD add more arguments by passing values to the load routine.
	// These could be just flag/help pairs

	// parse the arguments
	var clargs = parser.parseArgs();

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * 
	 * READ CONFIGURATION FILE(S)
	 * 
	 * Read in values declared from JSON configuration files. 
	 * 
	 * TODO: allow for INI style specs too. They are more readable and less finicky. 
	 * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	// if there is a .env file, load it
	if( _fs.existsSync( `${process.cwd()}/.env` ) ) {
		let result = require( 'dotenv' ).config();
		if( result.error ) { throw result.error; }
	}

	// get a "safe" specification of the configuration file: 
	// 
	// 		"--conf ..." > "YENACCESS_CONF_FILE = ..." > "conf.json"
	// 
	let confFile = "conf.json" , specifiedConfFile = false;
	if( 'conf_file' in clargs && clargs.conf_file ) { // preferentially use CL arg if provided
		confFile = clargs.conf_file;
		specifiedConfFile = true;
	} else { // see if there is an environment variable pointing to configuration file
		let cfenvvar = `${prefix}_CONF_FILE`;
		if( cfenvvar in process.env && process.env[cfenvvar] ) {
			confFile = process.env[cfenvvar];
			specifiedConfFile = true;
		}
	}

	// load configuration file thus specified
	// NOTE: we can just "require" a JSON file in node... given correct path conventions
	if( _fs.existsSync( confFile ) ) {
		_config = { ..._config , ...require( confFile ) };
	}

	// override config file settings with any environment variables declared
	/*
	const envvarsmap = {};
	defvars.forEach( v => {
		envvarsmap[ `${prefix}_${v}` ] = v.toLowerCase().replace('_','-');
	} );

	// look for existing environment variables with the declared values
	Object.keys( envvarsmap ).forEach( k => {
		if( process.env[k] ) { // does the variable exist? 
			var kks = envvarsmap[k].split( '-' ); 
			// assign values, nesting if appropriate
			if( kks.length == 2 ) { 
				if( ! _config[kks[0]] ) { _config[kks[0]] = {}; }
				_config[kks[0]][kks[1]] = process.env[k]; 
			} else { _config[kks[0]] = process.env[k]; }

		}
	} );
	*/

	// look for _existing_ environment variables with matching prefix
	// store data in the configuration using dasshes, or using underscores?
	let envvarre = new RegExp( `^${prefix}_` );
	Object.keys( process.env ).forEach( k => {
		if( envvarre.test(k) ) {
			let key = k.replace(envvarre,'').toLowerCase(); // .replace('_','-'); storing underscores
			_config[ key ] = process.env[k];
		}
	} );

	// overwrite/append any collection names in the config with environment variable values
	if( `${prefix}_MDB_NAMES` in process.env ) {
		_config.mdb_names = { 
			..._config['mdb-names'] , 
			...keyValueStringToObj( process.env[`${prefix}_MDB_NAMES`] ) 
		};
	}

	// parse command line options next
	Object.keys( clargs ).forEach( k => {
		_config[k] = clargs[k];
	} );

	// handle --dry-run differently (we could just assign, but this also filters)
	if( clargs.dry_run ) { _config.dry_run = true; }

	// overwrite collection convert "names" to an object and ... WHAT?
	if( clargs.mdb_names ) {
		_config.mdb_names = keyValueStringToObj( clargs.mdb_names );
	}

	// package defaults... 
	if( ! _config.port ) { _config.port = CONFIGURED_DEFAULT_PORT; }

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * 
	 * VALIDATE PREPARED CONFIGURATION
	 * 
	 * This is wrapped in a Promise to enable asynchronous checks.
	 * 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	return new Promise( ( resolve , reject ) => {

		// port must be available... 
		// await? this returns a Promise. I don't see any difference in testing. 
		isPortValidAndOpen( _config.port )
			.then( () => {

				// check that we actually read configuration file specified (presuming this isn't default)
				if( specifiedConfFile ) {
					if( ! _fs.existsSync( confFile ) ) {
						return reject( `Specified a configuration file (${confFile}) that does not exist` );
					}
				}

				// if ssl spec provided, those files must exist. do we need ALL three files, always?
				let sslSpec = vdk( 'ssl_key' , _config ) && vdk( 'ssl_cert' , _config ) && vdk( 'ssl_chain' , _config );
				if( sslSpec ) {
					if( ! _fs.existsSync( _config.ssl_key ) ) {
						return reject( `SSL configured, but the given file (${_config.ssl_key}) does not exist` );
					}
					if( ! _fs.existsSync( _config.ssl_cert ) ) {
						return reject( `SSL configured, but the given file (${_config.ssl_cert}) does not exist` );
					}
					if( ! _fs.existsSync( _config.ssl_chain ) ) {
						return reject( `SSL configured, but the given file (${_config.ssl_chain}) does not exist` );
					}
				}

				// can we check the mongodb URI? certainly if we _try_ a connection, but ostensibly we'll 
				// be doing that anyway


				// Set default values given any specification passed here. 
				if( defaults ) {
					Object.keys( defaults ).forEach( k => {
						if( ! vdk( k , _config ) ) { _config[k] = defaults[k]; }
					} );
				}

				// Evaluate (literally) any checks passed in for the environment and command line arguments
				if( checks ) {

					Object.keys( checks ).forEach( k => {
						let key = k.replace('-','_');
						if( vdk( key , clargs ) ) {
							let val = clargs[key]; // "val" can be referenced in the check
							try {
								if( ! eval( checks[k] ) ) {
									return reject( `Configuration parameter ${k} failed to pass a supplied check routine.` );
								}
							} catch( err ) {
								return reject( `Check on parameter ${k} threw an error: ${err.toString()}.` );
							}
						} else {
							return reject( `Required configuration parameter ${k} is missing.` ); 
						}
					} );

				}

				// make sure "nestOn" is well-formed
				if( ! nestOn ) {
					nestOn = [ 'ssl' , 'mdb' ];
				} else {
					nestOn.push( 'ssl' ); 
					nestOn.push( 'mdb' );
				}

				// prepare nested data structure
				nestOn = nestOn.map( n => {
					if( _config[n] ) {
						_config[n] = { '_' : _config[n] }; 
					} else {
						_config[n] = {}; 
					}
					return { n : n , r : ( new RegExp( `^${n}_` ) ) };
				} );

				// now, nest config variables on the nesting keys
				Object.keys( _config ).forEach( k => {
					nestOn.forEach( n => {
						if( n.r.test( k ) ) {
							_config[n.n][ k.replace( n.r , '' ) ] = _config[k];
							delete _config[k];
						}
					} );
				} )

				// now that everything is checked, nest configuration params with "." in them
				/*
				Object.keys( _config ).forEach( k => {
					let expanded = dotextract( k , _config[k] );
					inject( _config , expanded );
				} );
				*/

				// if we are here, all's good and we can return the config
				resolve( _config );

			} ).catch( ( err ) => { 
				console.log( err )
				reject( `Specified port ${_config.port} is in use.` ); 
			} );

	} );

}
