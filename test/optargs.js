
let optargs = {
	'custom-flag' : { help : 'An extra flag' , action : "storeTrue" , default : false } , 
	'custom-arg' : { help : 'An extra argument' , default : 7 } , 
}

let checks = { 
	'custom-arg' : 'val >= 2'
}

let exit_code = 0;
require( './../src/index.js' )( optargs , null , checks )
	.then( c => { console.log( "SUCCESS:: created config" , c ); } )
	.catch( err => { console.log( "FAILURE:: " , err ); exit_code = 1; } )
	.finally( process.exit )

// keeps process from exiting
process.stdin.resume();