
let exit_code = 0;
require( './../src/index.js' )()
	.then( c => { console.log( "SUCCESS:: created config" , c ); } )
	.catch( err => { console.log( "FAILURE:: " , err ); exit_code = 1; } )
	.finally( process.exit )

// keeps process from exiting
process.stdin.resume();