#!/bin/bash

# all defaults test
node test/general.js

# invalid arg test
node test/general.js --port 

# 
node test/general.js --port 7000 --dry-run
node test/general.js --port 8000 --dry-run

# some fake certs, should fail
node test/general.js --port 7000 --ssl-cert fake.cert --ssl-key fake.key 

# specifying configuration file
node test/general.js --conf-file conf.json
