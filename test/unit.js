
let index = 0;
let stats = { s : 0 , f : 0 };
let tests = [];

// increment the indexx and run the next test, if there is a next test
const proceedWhenDone = () => {
	if( index == tests.length - 1 ) { process.exit(); }
	else { index += 1; tests[index](); }
}

tests.push( () => {
	require( './../src/index.js' )()
		.then( c => { stats.s += 1; console.log( "success! config: " , c ); } )
		.catch( err => { stats.f += 1; console.log( "failure. " , err ); } )
		.finally( proceedWhenDone );
} );

tests[index]();

// keeps process from exiting until we're really done
process.stdin.resume();